<?php get_header(); ?>
    <div class="main-content">
        <div class="content-wrapper">
            <div class="content">
                <h1 class="title-page"><?php the_title(); ?></h1>
                <div class="posts-list">
                    <?php if( have_posts()) : while ( have_posts()) : the_post(); ?>
                        <div class="post-wrap">
                            <div class="post-thumbnail">

                                <img src="<?php echo get_pic($post->ID); ?>" alt="Image поста" class="post-thumbnail__image">
                            </div>
                            <div class="post-content">
                                <div class="post-content__post-info">
                                    <div class="post-date"><?php the_date(); ?></div>
                                </div>
                                <div class="post-content__post-text">
                                    <div class="post-title">
                                        <?php the_title(); ?>
                                    </div>
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="post-content__post-control">
                                    <a href="<?php the_permalink(); ?>"
                                       class="btn-read-post">Читать далее >></a></div>
                            </div>
                        </div>
                    <?php endwhile; else: ?>
                        Ничего не найдено
                    <?php endif; ?>
                </div>

                    <?php
                    $args = array(
                        'show_all'     => false, // показаны все страницы участвующие в пагинации
                        'end_size'     => 1,     // количество страниц на концах
                        'mid_size'     => 1,     // количество страниц вокруг текущей
                        'prev_next'    => true,  // выводить ли боковые ссылки "предыдущая/следующая страница".
                        'prev_text'    => __('«'),
                        'next_text'    => __('»'),
                        'add_args'     => false, // Массив аргументов (переменных запроса), которые нужно добавить к ссылкам.
                        'add_fragment' => '',     // Текст который добавиться ко всем ссылкам.
                        'screen_reader_text' => __( ' ' ),
                    );
                    the_posts_pagination($args);
                    ?>
            </div>
            <!-- sidebar-->
            <div class="sidebar">
                <div class="sidebar__sidebar-item">
                    <div class="sidebar-item__title">Теги</div>
                    <div class="sidebar-item__content">
                        <ul class="tags-list">
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">путешествия по россии</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">турция</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">гоа</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">авиабилеты</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">отели</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">европа</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">азия</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">тайланд</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">хостелы</a></li>
                            <li class="tags-list__item"><a href="#" class="tags-list__item__link">шоппинг</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar__sidebar-item">
                    <div class="sidebar-item__title">Категории</div>
                    <div class="sidebar-item__content">
                        <ul class="category-list">
                            <li class="category-list__item"><a href="#" class="category-list__item__link">
                                    Вылеты из
                                    столиц</a>
                                <ul class="category-list__inner">
                                    <li class="category-list__item"><a href="#" class="category-list__item__link">Москва</a></li>
                                    <li class="category-list__item"><a href="#" class="category-list__item__link">Санкт-Петербург</a></li>
                                </ul>
                            </li>
                            <li class="category-list__item"><a href="#" class="category-list__item__link">
                                    Вылеты из
                                    регионов</a>
                                <ul class="category-list__inner">
                                    <li class="category-list__item"><a href="#" class="category-list__item__link">Москва</a></li>
                                    <li class="category-list__item"><a href="#" class="category-list__item__link">Санкт-Петербург</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>