#!/usr/bin/env bash
DB="turistik"
CURHOST="turistik.loft"
PRODHOST="release.host.name"
mysqldump -uroot $DB > $DB.sql
cp $DB.sql $DB-prod.sql
sed -i s/$CURHOST/$PRODHOST/g $DB-prod.sql
