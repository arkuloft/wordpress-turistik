#!/usr/bin/env bash
DB="wp_loftschool"
mysql -uroot $DB -e "drop schema $DB"
mysql -uroot -e "create schema $DB"
mysql -uroot $DB < $DB.sql
